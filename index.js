$(function(){

    $("#newsite_text").fitText(1.46);
    $("#descript_text").fitText(1.79);

    $("#submitMessage").click(function(event){
      if(!$("#messageName").val() || !$("#messageContent").val() || !$("#messageEmail").val())
        toastr["error"]("All fields must be non-empty.");
      else if(!isEmail($("#messageEmail").val())){
        toastr["error"]("Email address is invalid.");
      }else{
        if($("#messaged0n0tf1llth1s1fy0ur4hum4n").val()) //é um bot, mas mostrar mensagem de sucesso pra enganar ele se for um mais espertinho
          toastr["success"]("Message sent successfully! We'll get back to you shortly.");
        else{
          handleFormSubmit(event)
        }
      }
    })
    $(".currentYear").text(new Date().getFullYear());
  })
  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  
  function getFormData(form) {
    var elements = form.elements;
  
    var fields = Object.keys(elements).map(function(k) {
      if(elements[k].name !== undefined) {
        return elements[k].name;
      // special case for Edge's html collection
      }else if(elements[k].length > 0){
        return elements[k].item(0).name;
      }
    }).filter(function(item, pos, self) {
      return self.indexOf(item) == pos && item;
    });
  
    var formData = {};
    fields.forEach(function(name){
      var element = elements[name];
      
      // singular form elements just have one value
      formData[name] = element.value;
  
      // when our element has multiple items, get their values
      if (element.length) {
        var data = [];
        for (var i = 0; i < element.length; i++) {
          var item = element.item(i);
          if (item.checked || item.selected) {
            data.push(item.value);
          }
        }
        formData[name] = data.join(', ');
      }
    });
  
    // add form-specific values into the data
    formData.formDataNameOrder = JSON.stringify(fields);
    formData.formGoogleSheetName = form.dataset.sheet || "responses"; // default sheet name
    formData.formGoogleSendEmail
      = form.dataset.email || ""; // no email by default
  
    return formData;
  }
  
  function handleFormSubmit(event) {  // handles form submit without any jquery
    event.preventDefault();           // we are submitting via xhr below
    var form =  document.getElementsByClassName("gform")[0];
    var data = getFormData(form);
    $("#submitMessage").prop('disabled',true)
    $("#submitMessage").text('Please wait...')
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://script.google.com/a/moonsailor.com/macros/s/AKfycbyh1G7ZiEOp24XcLdt88VWCp-C_o699n7FR13kshQ/exec');
    // xhr.withCredentials = true;
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function() {
        $("#messageName").val('')
        $("#messageContent").val('')
        $("#messageEmail").val('')
        $("#submitMessage").prop('disabled',false)
        $("#submitMessage").text('Submit ->')
      if (xhr.readyState === 4 && xhr.status === 200) {
        toastr["success"]("Message sent successfully! We'll get back to you shortly.");
      }
    };
    // url encode form data for sending as post data
    var encoded = Object.keys(data).map(function(k) {
        return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
    }).join('&');
    xhr.send(encoded);
  }